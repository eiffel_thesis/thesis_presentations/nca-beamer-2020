\newif \ifshare
% \sharetrue % Comment this if you want animation
\ifshare % "Share mode" without animation.
\documentclass[table, trans, aspectratio = 169]{beamer}
\else % "Presentation mode" with animation.
\documentclass[table, aspectratio = 169]{beamer}
\fi
\usepackage{default}
\usepackage[T1]{fontenc}
\usepackage{color}
\usepackage{siunitx}
\usepackage{pgfplots}
\usepackage{algpseudocode}
\usepackage{rotating}
\usepackage{bytefield}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{pifont}
\usepackage{diagbox}


\let\pgfmathMod=\pgfmathmod\relax

\sisetup{binary-units=true}

\graphicspath{{figs/PDF/}}

\usetheme{Boadilla}

\definecolor{struct}{HTML}{03a9f4}
\definecolor{alert}{HTML}{f44336}
\definecolor{example}{HTML}{aeea00}
\definecolor{good}{HTML}{8bc34a}
\definecolor{notgoodnotbad}{HTML}{ff9800}

\setbeamercolor{structure}{fg = struct}
\setbeamercolor{normal text}{fg = white, bg = black}
\setbeamercolor{example text}{fg = example}
\setbeamercolor{alerted text}{fg = alert}
\setbeamercolor{footline}{fg = white}

\title[NCA 2020]{\textbf{NCA 2020}\\MemOpLight: Leveraging application feedback to improve container memory consolidation}
\author[LIP6 researchers]{\underline{Francis Laniel} (\texttt{francis.laniel@lip6.fr})\\Damien Carver (\texttt{damien.carver@lip6.fr})\\Julien Sopena (\texttt{julien.sopena@lip6.fr})\\Franck Wajsburt (\texttt{franck.wajsburt@lip6.fr})\\Jonathan Lejeune (\texttt{jonathan.lejeune@lip6.fr})\\Marc Shapiro (\texttt{marc.shapiro@acm.org})}
\date{November 2020}

% This need the pifont package.
% Information was taken from: https://tex.stackexchange.com/a/42620.
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\newcommand{\step}[3]{$\varphi_{#1}(#2, #3)$}
\newcommand{\stepeight}[1]{$\varphi_#1$}

% Custom title page.
\defbeamertemplate*{title page}{customized}[1][]{
	\centering
	\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle\par
	\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par
	\bigskip
	\usebeamerfont{author}\usebeamercolor[fg]{normal text}\textbf{\insertauthor}\par
	\bigskip
	\usebeamerfont{date}\usebeamercolor[fg]{normal text}\textbf{\insertdate}\par
	\bigskip
	\bigskip

	% Makebox takes two options:
	% 1. First is the width, setting 0 means that the box has the size of the
	% content.
	% 2. Second is the text alignement.
	% We use it to display 3 elements aligned correctly.
	% This tip was taken from:
	% https://tex.stackexchange.com/a/223348
	\makebox[0pt][l]{\includegraphics[scale = .5]{sorbonne.pdf}}%
	\hfill%
	\makebox[0pt][c]{\includegraphics[scale = .05]{lip6.pdf}}%
	\hfill%
	\makebox[0pt][r]{\includegraphics[scale = .5]{inria.pdf}}%
}

\begin{document}
	% Put these parameters here to avoid compilation error:
	% "! LaTeX Error: Missing \begin{document}."
	% Remove the navigation bar, this is useless...
	\setbeamertemplate{navigation symbols}{}
	% Use square instead of bubbles, see:
	% https://tex.stackexchange.com/a/69721
	\setbeamertemplate{section in toc}[square]
	% Modify the shaded value to 60% instead of 20%, see:
	% https://tex.stackexchange.com/a/66703
	\setbeamertemplate{sections/subsections in toc shaded}[default][50]
	% Use circle instead of bubbles for itemize, see:
	% \setbeamertemplate{itemize items}[circle]
	\setbeamertemplate{itemize items}[circle]

	\maketitle

	\section{Le \textit{cloud}}
	\begin{frame}
		\frametitle{The cloud}
		\framesubtitle{Introduction}

		The cloud consists in \cite{amazon_quest_ce_nodate}:
		\begin{quote}
			[...] the on-demand delivery of IT resources over the Internet with pay-as-you-go pricing.
		\end{quote}

		\bigskip

		\onslide<2->{
			Divergent objectives:
			\begin{itemize}
				\item \textcolor{struct}{For the client:} Access the resources he/she paid for.
				\item \textcolor{struct}{For the provider:} Maximize the number of clients for a given number of resources while ensuring QoS.
			\end{itemize}
		}

		\bigskip

		\onslide<3->{
			\textcolor{struct}{SLA:} Service Level Agreement.

			\textcolor{struct}{SLO:} Service Level Objective.
		}

		\bigskip

		\only<4>{
			\centering

			\textcolor{struct}{How to ensure SLA while maximizing number of clients for given resources?}
		}
	\end{frame}

	\begin{frame}
		\frametitle{Containers and memory}

		\begin{columns}
			\begin{column}{.5\textwidth}
				Containers are based on several kernel features:
				\begin{description}
					\item[Les \texttt{namespaces}:] Security isolation \cite{rami_rosen_namespace_2016, kerrisk_lce_2012, kerrisk_namespaces_2013}.
					\item[Les \texttt{cgroups}:] Resources isolation \cite{rami_rosen_namespace_2016, hiroyu_cgroup_2008}.
				\end{description}

				\bigskip

				The memory \texttt{cgroup} offers two limits:
				\begin{itemize}
					\item \texttt{max limit:} Impossible to exceed.
					\item \texttt{soft limit:} Impossible to exceed under memory pressure.
				\end{itemize}
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[scale = .42]{container.pdf}

				Container (\texttt{docker}, \texttt{lxc}, \texttt{podman}, etc.)
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Memory management}
		\framesubtitle{Anoymous memory \textit{vs.} file memory}

		\includegraphics<1>[scale = .4]{memory_cache-fig1.pdf}%
		\includegraphics<2>[scale = .4]{memory_cache-fig2.pdf}%
		\includegraphics<3>[scale = .4]{memory_cache-fig3.pdf}%
		\includegraphics<4->[scale = .4]{memory_cache-fig4.pdf}%

		\begin{scriptsize}
			\begin{columns}
				\begin{column}{.5\textwidth}<2->
					Anoymous memory:
					\begin{itemize}
						\item Used to store code, stack and heap (\texttt{malloc}).
						\item If impossible to allocate, the applications stops.
					\end{itemize}
				\end{column}
				\begin{column}{.5\textwidth}<4>
					File memory:
					\begin{itemize}
						\item Used as cache for data read from disk.
						\item Use up to all no allocated memory.
					\end{itemize}
				\end{column}
			\end{columns}
		\end{scriptsize}
	\end{frame}

	\begin{frame}
		\frametitle{The memory}
		\framesubtitle{Memory management with \texttt{cgroups} \cite{corbet_integrating_2011, rik_van_riel_patch_2008}}

		\includegraphics<1>[scale = .4]{memory_cache-fig4.pdf}%
		\includegraphics<2>[scale = .4]{memory_cache-fig5.pdf}%
		\includegraphics<3>[scale = .4]{memory_cache-fig6.pdf}%
		\includegraphics<4>[scale = .4]{memory_cache-fig7.pdf}%
		\includegraphics<5>[scale = .4]{memory_cache-fig8.pdf}%

		\only<5>{
			Problem:
			\begin{itemize}
				\item When memory pressure occurs, all \texttt{cgroups} are reclaimed.
				\item It is impossible to target one \texttt{cgroup}.
			\end{itemize}
		}
	\end{frame}

	\begin{frame}
		\frametitle{Isolation without consolidation}
		\framesubtitle{Definition}

		\begin{columns}
			\begin{column}{.33\textwidth}
				\centering

				No limits set

				\bigskip

				\includegraphics<1>[scale = .25]{no_limit-fig1.pdf}%
				\includegraphics<2>[scale = .25]{no_limit-fig2.pdf}%
				\includegraphics<3->[scale = .25]{no_limit-fig3.pdf}%
			\end{column}
			\begin{column}{.33\textwidth}<4->
				\centering

				\texttt{Max} limit

				\bigskip

				\includegraphics<4>[scale = .25]{max_limit-fig1.pdf}%
				\includegraphics<5->[scale = .25]{max_limit-fig2.pdf}%
			\end{column}
			\begin{column}{.33\textwidth}<6->
				\centering

				\texttt{Soft} limit

				\bigskip

				\includegraphics<6>[scale = .25]{soft_limit-fig1.pdf}%
				\includegraphics<7>[scale = .25]{soft_limit-fig2.pdf}%
				\includegraphics<8->[scale = .25]{soft_limit-fig3.pdf}%
			\end{column}
		\end{columns}

		\bigskip

		\only<9>{
			\centering

			\textcolor{struct}{What is the effect of these mechanisms with regard to isolation and consolidation?}
		}
	\end{frame}

	\begin{frame}
		\frametitle{Isolation without consolidation}
		\framesubtitle{Experimental setup \cite{alexey_kopytov_sysbench_nodate, thesis_sysbench, noauthor_mysql_nodate, grid5000, intel_intel_2017, qemu_qemu_2019, qemu_qemu_2019, noauthor_docker_py_nodate}}

		\centering

		\includegraphics<1>[scale = .33]{experiment_setup-fig1.pdf}%
		\includegraphics<2>[scale = .33]{experiment_setup-fig2.pdf}%
		\includegraphics<3>[scale = .33]{experiment_setup-fig3.pdf}%
		\includegraphics<4>[scale = .33]{experiment_setup-fig4.pdf}%
		\includegraphics<5>[scale = .33]{experiment_setup-fig5.pdf}%
	\end{frame}

	\begin{frame}
		\frametitle{Isolation without consolidation}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[scale = .42]{dummy_transactions-fig1.pdf}%
				\includegraphics<2>[scale = .42]{dummy_transactions-fig2.pdf}%
				\includegraphics<3>[scale = .42]{dummy_transactions-fig3.pdf}%
				\includegraphics<4>[scale = .42]{dummy_transactions-fig4.pdf}%

				Throughput
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[scale = .42]{dummy_memory-fig1.pdf}%
				\includegraphics<2>[scale = .42]{dummy_memory-fig2.pdf}%
				\includegraphics<3->[scale = .42]{dummy_memory-fig3.pdf}%

				Memory
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Isolation without consolidation}
		\framesubtitle{Experimental results}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig6.pdf}%
				\includegraphics<7->[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig7.pdf}%

				Throughput
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig6.pdf}%
				\includegraphics<7->[width = \textwidth, height = .35\textheight]{pair_default_memory-fig7.pdf}%

				Memory
			\end{column}
		\end{columns}

		\vspace{-.6cm}

		\begin{center}
			No limits set
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig7.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig7.pdf}%

				Throughput
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig7.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig7.pdf}%

				Memory
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			\texttt{\onslide<-7>{Max}\only<8>{Soft}} limit
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Isolation without consolidation}
		\framesubtitle{Conclusion}

		\begin{table}
			\centering

			\begin{tabular}{|l|c|c|}
				\hline
				Used  & Consolidation & Isolation\\
				\hline
				no limit & \textcolor{notgoodnotbad}{weak} & \textcolor{alert}{no}\\
				\hline
				\texttt{max} limit & \textcolor{alert}{no} & \textcolor{good}{yes}\\
				\hline
				\texttt{soft} limit & \textcolor{notgoodnotbad}{no (under memory pressure)} & \textcolor{good}{yes}\\
				\hline
			\end{tabular}
		\end{table}

		\bigskip

		\onslide<2->{
			\centering

			The \texttt{soft limit} helps to consolidate unused memory, but does not help with underused memory.
		}

		\bigskip

		\only<3>{
			\centering

			\textcolor{struct}{How to combine isolation while consolidating underused memory?}
		}
	\end{frame}

	\begin{frame}
		\frametitle{MemOpLight}
		\framesubtitle{Inside User and Kernel!}

		\begin{columns}
			\begin{column}{.6\textwidth}
				\includegraphics<1>[scale = .2]{memoplight-fig1.pdf}%
				\includegraphics<2>[scale = .2]{memoplight-fig2.pdf}%
				\includegraphics<3>[scale = .2]{memoplight-fig3.pdf}%
				\includegraphics<4>[scale = .2]{memoplight-fig4.pdf}%
				\includegraphics<5>[scale = .2]{memoplight-fig5.pdf}%
				\includegraphics<6->[scale = .2]{memoplight-fig6.pdf}%
			\end{column}
			\begin{column}{.4\textwidth}<7>
				\begin{tabular}{ccc}
					\hline
					Red & Yellow & Reclaimed\\
					\hline
					$0$ & $0$ & none\\
					$0$ & $\ge 1$ & green\\
					$\ge 1$ & $0$ & green\\
					$\ge 1$ & $\ge 1$ & green then yellow\\
					\hline
				\end{tabular}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Evaluation}
		\framesubtitle{With two containers}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig7.pdf}%

				Throughput
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig7.pdf}%

				Memory
			\end{column}
		\end{columns}

		\vspace{-.6cm}

		\begin{center}
			\texttt{Soft} limit
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig7.pdf}%

				Throughput
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig7.pdf}%

				Memory
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			MemOpLight
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Evaluation}
		\framesubtitle{With two containers: conclusion}

		\begin{small}
			\begin{table}
				\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
					\hline
					Phase & \multicolumn{2}{c|}{\step{1}{h}{h}} & \multicolumn{2}{c|}{\step{2}{h}{l}} & \multicolumn{2}{c|}{\step{3}{h}{i}} & \multicolumn{2}{c|}{\step{4}{l}{l}} & \multicolumn{2}{c|}{\step{5}{i}{h}} & \step{6}{s}{h}\\
					\hline
					Container & A & B & A & B & A & B & A & B & A & B & B\\
					\hline
					Load & 2000 & 2000 & 2000 & 200 & 2000 & 1500 & 200 & 200 & 1500 & 2000 & 2000\\
					\hline
					No limit & 1053 & \textcolor{struct}{1043} & 1374 & 196 & 1054 & \textcolor{struct}{1168} & 195 & 197 & 1195 & \textcolor{struct}{1145} & \textcolor{struct}{1751}\\
					\texttt{Max} limit & 1290 & 894 & 1411 & 196 & 1314 & 968 & 196 & 660 & 1358 & 978 & 962\\
					\texttt{Soft} limit & 1268 & 879 & 1423 & 197 & 1299 & 969 & 196 & 794 & 1360 & 970 & 974\\
					MemOpLight & \textcolor{struct}{1351} & 933 & \textcolor{struct}{1670} & 195 & \textcolor{struct}{1362} & 999 & 196 & 197 & \textcolor{struct}{1370} & 1036 & 1723\\
				\end{tabular}

				Throughput
			\end{table}
		\end{small}

		\only<2->{Conclusions:}
		\begin{itemize}
			\item<3-> MemOpLight is the best in all cases...
			\item<4> Except for container B when executed without limit, but this is not desirable!
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Evaluation}
		\framesubtitle{Eight containers: setup}

		\begin{columns}
			\begin{column}{.4\textwidth}
				Eight containers (A to H):
				\begin{itemize}
					\item Are runned in VM with \SI{6}{\giga\byte}.
					\item Use both two CPU cores.
					\item Use both \texttt{sysbench oltp} benchmark which interacts with \texttt{mysql 5.7.0} \cite{alexey_kopytov_sysbench_nodate, thesis_sysbench, noauthor_mysql_nodate}.
					\item Read, each, a \SI{2}{\giga\byte} database.
					\item Container loads vary each \SI{120}{\second}.
				\end{itemize}
			\end{column}
			\begin{column}{.6\textwidth}<2>
				\begin{table}
					\begin{tabular}{ccc}
						\hline
						Step & Phase(s) & Load\\
						\hline
						1 & \stepeight{1} et \stepeight{2} & High\\
						2 & \stepeight{3} & Decreasing\\
						3 & \stepeight{4} & Low\\
						4 & \stepeight{5} to \stepeight{9} & Increasing\\
						\hline
					\end{tabular}
				\end{table}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Evaluation}
		\framesubtitle{Eight containers: results}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig2.pdf}%
				\includegraphics<3,4>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig3.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig4.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig5.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig6.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig7.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig8.pdf}%

				Transactions
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<4,5>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig1.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig2.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig3.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig4.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig5.pdf}%

				\onslide<4->{Satisfaction}
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			\texttt{Soft} limit
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig2.pdf}%
				\includegraphics<3,4>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig3.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig4.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig5.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig6.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig7.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig8.pdf}%

				Transactions
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<4,5>[width = \textwidth, height = .35\textheight]{mechanism_color-fig1.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{mechanism_color-fig2.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{mechanism_color-fig3.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{mechanism_color-fig4.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{mechanism_color-fig5.pdf}%

				\onslide<4->{Satisfaction}
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			MemOpLight
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Evaluation}
		\framesubtitle{Eight containers: conclusion}

		\begin{table}
			\centering

			\begin{tabular}{l|cccccccc|c}
				\hline
				\diagbox{Mechanism}{Container} & A & B & C & D & E & F & G & H & Total\\
				\hline
				No limit & 15.2 & 14.5 & 17.3 & 11.9 & 18.7 & 15.9 & \textcolor{struct}{8.8} & \textcolor{struct}{10.3} & 112.6\\
				\texttt{Max} limit & 13.6 & 13.8 & 13.8 & 8.5 & 14.9 & 9.9 & 4.6 & 6.8 & 85.9\\
				\texttt{Soft} limit & 17.1 & \textcolor{struct}{17.4} & 17.6 & 12.6 & 17.5 & 13.4 & 7.3 & 9.5 & 112.4\\
				MemOpLight & \textcolor{struct}{17.5} & 17.3 & \textcolor{struct}{18.4} & \textcolor{struct}{13.7} & \textcolor{struct}{20.2} & \textcolor{struct}{16.8} & 8.6 & 10.1 & \textcolor{struct}{122.6}\\
			\end{tabular}

			Transactions (millions)
		\end{table}

		\vspace{-.5cm}

		\begin{table}
			\centering

			\begin{tabular}{l|cccccccc|c}
				\hline
				\diagbox{Mechanism}{Container} & A & B & C & D & E & F & G & H & Total\\
				\hline
				No limit & 42\% & 23\% & 8\% & 35\% & 69\% & 39\% & 62\% & \textcolor{struct}{71\%} & 44\%\\
				\texttt{Max} limit & \textcolor{struct}{60\%} & 35\% & 5\% & 37\% & 73\% & 15\% & 53\% & 35\% & 39\%\\
				\texttt{Soft} limit & 56\% & 42\% & 11\% & 35\% & 64\% & 16\% & 53\% & 32\% & 39\%\\
				MemOpLight & 58\% & \textcolor{struct}{44\%} & \textcolor{struct}{42\%} & \textcolor{struct}{52\%} & \textcolor{struct}{76\%} & \textcolor{struct}{53\%} & \textcolor{struct}{67\%} & 67\% & \textcolor{struct}{57\%}\\
			\end{tabular}

			Satisfaction (\%)
		\end{table}
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}

		Contributions:
		\begin{itemize}
			\item We showed the container problem: isolation without consolidation.
			\item We designed the MemOpLight algorithm which is based on application feedback
			\item We developed application probes to measure containers' performance.
			\item We modified Linux kernel memory reclaimation mechanism to take into account container performance.
			\item We studied MemOpLight parameters (percent of memory reclaimed, reclaimation period, reclaiming or not yellow containers).
			\item MemOpLight increases performance up to 8.9\% and satisfaction of 13\%.
		\end{itemize}
	\end{frame}

	\begin{frame}[allowframebreaks, noframenumbering]
		\frametitle{Bibliography}
		\setbeamertemplate{bibliography item}[text]

		\begin{scriptsize}
			\bibliographystyle{alpha}
			\bibliography{nca}

			\bigskip
			Some images of this presentation are based on picture of the following authors: \href{https://www.flaticon.com/authors/smashicons}{Smashicons}, \href{https://www.flaticon.com/authors/good-ware}{Good Ware}, \href{https://icon54.com/}{Icons mind}, \href{https://www.flaticon.com/authors/simpleicon}{SimpleIcon}, \href{https://www.sketchappsources.com/contributor/solarez}{Norman Posselt}, \href{https://www.flaticon.com/fr/auteurs/nikita-golubev}{Nikita Golubev} and \href{http://www.freepik.com}{Freepik} from \href{http://www.flaticon.com}{Flaticon}.
		\end{scriptsize}
	\end{frame}
\end{document}